#ifndef MANAGECONSULTATIONSWINDOW_H
#define MANAGECONSULTATIONSWINDOW_H

#include <QMainWindow>

class CCConsultationRecord;

namespace Ui {
class ManageConsultationsWindow;
}

class ManageConsultationsWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit ManageConsultationsWindow(QWidget *parent = 0);
    ~ManageConsultationsWindow();
    bool addConsultationToTable(CCConsultationRecord* record);
    
private slots:
    void on_actionAdd_New_Consultation_triggered();

private:
    Ui::ManageConsultationsWindow *ui;
};

#endif // MANAGECONSULTATIONSWINDOW_H
