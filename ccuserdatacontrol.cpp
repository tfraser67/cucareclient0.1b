#include "ccuserdatacontrol.h"
#include <QString>

CCUserDataControl* SINGLETON = 0;

CCUserDataControl::CCUserDataControl() :
    _pUserName(""),
    _pPriviledgeLevel(CCUserPriviledgeLevelNoAccess)
{
}

CCUserDataControl::~CCUserDataControl()
{
}

CCUserDataControl* CCUserDataControl::sharedUserDataControl()
{
    if (SINGLETON == 0)
    {
        SINGLETON = new CCUserDataControl();
    }

    return SINGLETON;
}
