#ifndef LOGINWINDOW_H
#define LOGINWINDOW_H

#include <QMainWindow>
#include "homewindow.h"
#include "systemhomewindow.h"

namespace Ui {
class LoginWindow;
}

class LoginWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit LoginWindow(QWidget *parent = 0);
    ~LoginWindow();
    void ShowLoginScreen();
    
private slots:
    void on_pushButton_clicked();

private:
    Ui::LoginWindow *ui;
    void ShowPhysicianHome();
    void ShowAdminAssistantHome();
    void ShowSystemAdminHome();
};

#endif // LOGINWINDOW_H
