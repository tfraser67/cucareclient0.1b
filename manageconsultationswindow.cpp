#include "manageconsultationswindow.h"
#include "ui_manageconsultationswindow.h"
#include "ccconsultationrecord.h"

ManageConsultationsWindow::ManageConsultationsWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ManageConsultationsWindow)
{
    ui->setupUi(this);
    ui->consultationTableWidget->resizeColumnsToContents();
    ui->consultationTableWidget->resizeRowsToContents();
    ui->consultationTableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);

    //**** TODO: Replace with fetch code ****//
    on_actionAdd_New_Consultation_triggered();
    on_actionAdd_New_Consultation_triggered();
}

ManageConsultationsWindow::~ManageConsultationsWindow()
{
    delete ui;
}

bool ManageConsultationsWindow::addConsultationToTable(CCConsultationRecord* record)
{
    int rowIndex = ui->consultationTableWidget->rowCount();
    ui->consultationTableWidget->insertRow(rowIndex);

    QString statusString;
    switch(record->status)
    {
        case CCConsultationRecordStatusOverdue:
            statusString = "Overdue";
            break;
        case CCConsultationRecordStatusPending:
            statusString = "Pending";
            break;
        case CCConsultationRecordStatusComplete:
            statusString = "Complete";
            break;
        default:
            statusString = "Unknown";
            break;
    }

    ui->consultationTableWidget->setItem(rowIndex, 0, new QTableWidgetItem(statusString));
    ui->consultationTableWidget->setItem(rowIndex, 1, new QTableWidgetItem(record->patientName));
    ui->consultationTableWidget->setItem(rowIndex, 2, new QTableWidgetItem(record->physicianName));
    ui->consultationTableWidget->setItem(rowIndex, 3, new QTableWidgetItem(record->clinicName));
    ui->consultationTableWidget->setItem(rowIndex, 4, new QTableWidgetItem(record->dateOfConsult.toString()));

    ui->consultationTableWidget->setVerticalHeaderItem(rowIndex, new QTableWidgetItem(""));

    ui->consultationTableWidget->resizeColumnsToContents();
    ui->consultationTableWidget->resizeRowsToContents();

    return true;
}

void ManageConsultationsWindow::on_actionAdd_New_Consultation_triggered()
{
    // Fake add for now
    //**** TODO: Change to widget creator ****//
    srand(time(0));

    CCConsultationRecord* record = new CCConsultationRecord("Fake Patient", "Dr. A. Nonymous", (CCConsultationRecordStatus)(rand() % 3), "Clinic of the Lost Ark", QDateTime::currentDateTime());
    this->addConsultationToTable(record);
}
