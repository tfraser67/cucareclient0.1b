#include "loginwindow.h"
#include "ui_loginwindow.h"

LoginWindow::LoginWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::LoginWindow)
{
    ui->setupUi(this);
}

LoginWindow::~LoginWindow()
{
    delete ui;
}

void LoginWindow::on_pushButton_clicked()
{
    this->close();
    switch (ui->userComboBox->currentIndex()){
        case 0:
            ShowPhysicianHome();
            break;
        case 1:
            ShowAdminAssistantHome();
            break;
        case 2:
            ShowSystemAdminHome();
            break;
    }
}

void LoginWindow::ShowLoginScreen(){
    this->show();
}

void LoginWindow::ShowPhysicianHome(){
    HomeWindow* home = new HomeWindow();
    home->show();
}

void LoginWindow::ShowAdminAssistantHome(){
    HomeWindow* home = new HomeWindow();
    home->show();
}
void LoginWindow::ShowSystemAdminHome(){
    SystemHomeWindow* home = new SystemHomeWindow();
    home->show();
}
