#include "ccconsultationrecord.h"

CCConsultationRecord::CCConsultationRecord(QString aPatientName, QString aPhysicianName, CCConsultationRecordStatus aStatus, QString aClinic, QDateTime aDate) :
    patientName(aPatientName), physicianName(aPhysicianName), status(aStatus), clinicName(aClinic), dateOfConsult(aDate)
{

}
