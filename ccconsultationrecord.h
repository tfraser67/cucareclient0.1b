#ifndef CCCONSULTATIONRECORD_H
#define CCCONSULTATIONRECORD_H

#include <QDateTime>
#include <QString>

enum CCConsultationRecordStatus
{
    CCConsultationRecordStatusPending = 0,
    CCConsultationRecordStatusComplete = 1,
    CCConsultationRecordStatusOverdue = 2
};

class CCConsultationRecord
{
public:
    explicit CCConsultationRecord(QString aPatientName, QString aPhysicianName, CCConsultationRecordStatus aStatus, QString aClinic, QDateTime aDate);

    CCConsultationRecordStatus status;
    QString physicianName;
    QString patientName;
    QString clinicName;
    QDateTime dateOfConsult;
};

#endif // CCCONSULTATIONRECORD_H
