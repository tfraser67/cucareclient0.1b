#-------------------------------------------------
#
# Project created by QtCreator 2012-10-24T18:21:28
#
#-------------------------------------------------

QT       += core gui

TARGET = COMP3004
TEMPLATE = app


SOURCES += main.cpp\
        homewindow.cpp \
    loginwindow.cpp \
    systemhomewindow.cpp \
    manageconsultationswindow.cpp \
    ccuserdatacontrol.cpp \
    ccconsultationrecord.cpp

HEADERS  += homewindow.h \
    loginwindow.h \
    systemhomewindow.h \
    manageconsultationswindow.h \
    ccuserdatacontrol.h \
    ccconsultationrecord.h

FORMS    += homewindow.ui \
    loginwindow.ui \
    systemhomewindow.ui \
    manageconsultationswindow.ui
