#ifndef CCUSERDATACONTROL_H
#define CCUSERDATACONTROL_H

#include <QString>

enum CCUserPriviledgeLevel
{
    CCUserPriviledgeLevelNoAccess = 0,
    CCUserPriviledgeLevelAdminAssistant = 1,
    CCUserPriviledgeLevelPhysician = 2,
    CCUserPriviledgeLevelSysAdmin = 3
};

class CCUserDataControl
{
public:
    CCUserDataControl();
    ~CCUserDataControl();
    CCUserDataControl* sharedUserDataControl();
private:
    QString _pUserName;
    CCUserPriviledgeLevel _pPriviledgeLevel;
};

#endif // CCUSERDATACONTROL_H
