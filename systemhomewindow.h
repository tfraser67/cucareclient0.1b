#ifndef SYSTEMHOMEWINDOW_H
#define SYSTEMHOMEWINDOW_H

#include <QMainWindow>

namespace Ui {
class SystemHomeWindow;
}

class SystemHomeWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit SystemHomeWindow(QWidget *parent = 0);
    ~SystemHomeWindow();
    
private:
    Ui::SystemHomeWindow *ui;
};

#endif // SYSTEMHOMEWINDOW_H
