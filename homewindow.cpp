#include "homewindow.h"
#include "ui_homewindow.h"
#include "manageconsultationswindow.h"

HomeWindow::HomeWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::HomeWindow)
{
    ui->setupUi(this);
}

HomeWindow::~HomeWindow()
{
    delete ui;
}

void HomeWindow::on_actionManage_Consultation_triggered()
{
    ManageConsultationsWindow* mcw = new ManageConsultationsWindow();
    if (mcw)
    {
        mcw->show();
    }
}
