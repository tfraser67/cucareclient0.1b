#include "systemhomewindow.h"
#include "ui_systemhomewindow.h"

SystemHomeWindow::SystemHomeWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SystemHomeWindow)
{
    ui->setupUi(this);
}

SystemHomeWindow::~SystemHomeWindow()
{
    delete ui;
}
