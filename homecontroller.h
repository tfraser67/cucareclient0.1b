#ifndef HOMECONTROLLER_H
#define HOMECONTROLLER_H

#include "loginwindow.h"
#include "homewindow.h"

class HomeController
{
public:
    HomeController();
    void ShowLoginScreen();

private:
    LoginWindow* login;
    HomeWindow* home;
};

#endif // HOMECONTROLLER_H
